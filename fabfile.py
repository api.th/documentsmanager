import os
from fabric.api import local
from plugins.vault import Vault, write_file

def pullsettings():
    if os.path.exists("settings.ini"):
        return
    vault = Vault()
    token = vault.read_secret_field("ddcbot/access_token")
    write_file(
        "settings.ini", 
        content="\n".join(
            [
                "[settings]",
                "ACCESS_TOKEN=%s" % token,
                "GIT_DIRECTORY=../../Documents"
            ]
        )
    )
    local("chmod 600 settings.ini")


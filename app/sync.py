import requests
import simplejson as json
import os
import re
import time
import subprocess
import argparse

from decouple import config

DEFAULT_TIMEOUT = 5

ACCESS_TOKEN = config("ACCESS_TOKEN")

QUERY = """
    query {
        list_registered_document (status: "50c", family__name: "QMS") {
            id
            number
            revision
            name
            get_viewer_file_attachment
        }
    }
"""


def get_auth_headers():
    return {
        "Authorization": 'TOKEN token="%s"' % ACCESS_TOKEN,
    }



QUERY_URL = "https://bangbon.api.co.th/workcenter-65/biggraph/query/v2"


class Document:
    def __init__(self, pk=None, number=None, revision=None, name=None, path=None):
        self.pk = pk
        self.number = number
        self.revision = revision
        self.name = name
        self.path = path

    def __str__(self):
        return "%s-%s at %s" % (self.number, self.revision, self.path)

    def get_path(self):
        FR, EG, _ = self.number.split("-", 2)
        cleanned_name = self.name.replace("/", "-")
        return os.path.join(FR, EG, "%s-%s %s.pdf" % (self.number, self.revision, cleanned_name))

def list_documents_from_server():
    result = requests.post(QUERY_URL, headers=get_auth_headers(), json={
        "query": QUERY,
    })
    content = json.loads(result.content)
    result = content.get("result")
    if result != "success":
        raise Exception(content)

    data = content.get("data")
    entries = data.get("list_registered_document") or {}
    for entry in entries:
        if not entry.get("get_viewer_file_attachment"):
            continue
        yield Document(
            pk=entry.get("id"),
            number=entry.get("number"),
            revision=entry.get("revision"),
            name=entry.get("name"),
        )


FILE_NAME_PATTERN = re.compile('(?P<number>[A-Z]+\-[A-Z]+\-[\d\.]+)\-(?P<revision>R[\d]+)\s*(?P<name>.*)\.pdf', re.IGNORECASE)

def list_documents_from_directory(directory):
    for root, dirs, files in os.walk(directory):
        for path in files:
            match = FILE_NAME_PATTERN.match(path)
            if not match:
                continue
            yield Document(
                number=match.group("number"),
                revision=match.group("revision"),
                name=match.group("name"),
                path=os.path.join(root, path),
            )

def download_url(url, file_name, timeout=None):    
    if not timeout:
        timeout = DEFAULT_TIMEOUT

    directory = os.path.dirname(file_name)
    if not os.path.exists(directory):
        os.makedirs(directory)

    auth_headers = get_auth_headers()
    response = requests.get(url, headers=auth_headers)
    time.sleep(timeout)

    if 'filemaker:' in url:
        _, bit = url.rsplit('filemaker:', 1)
        response = requests.get(response.url + '?intent=print:%s' % bit, headers=auth_headers)
        time.sleep(timeout)

    download_response = requests.get(response.url + '?intent=download', headers=auth_headers)
    with open(file_name, "wb") as f:
        f.write(download_response.content)


REMOTE_URL = "https://bangbon.api.co.th/workcenter-65/registereddocument-%s/print?intent=print:103"

def download_remote_file(remote_file, directory=None, timeout=None):    
    url = REMOTE_URL % remote_file.pk
    file_name = os.path.join(directory, remote_file.get_path())
    download_url(url, file_name, timeout=timeout)
    if os.stat(file_name).st_size == 0:
        os.remove(file_name)
        return False
    return True



def sync_directory(directory):
    local_files_by_number = {}
    for local_file in list_documents_from_directory(directory):
        local_files_by_number[local_file.number] = local_file

    changes = []
    remote_files_by_number = {}
    for remote_file in list_documents_from_server():
        remote_files_by_number[remote_file.number] = remote_file
        local_file = local_files_by_number.get(remote_file.number)
        if not local_file:
            action = "เพิ่ม %s-%s %s" % (remote_file.number, remote_file.revision, remote_file.name)
            print(action, "...")
            if download_remote_file(remote_file, directory=directory):
                changes.append(action)
            continue

        if local_file.revision == remote_file.revision:
            continue

        action = "แก้ไข %s %s %s → %s" % (remote_file.number, remote_file.name, local_file.revision, remote_file.revision)
        print(action, "...")
        if download_remote_file(remote_file, directory=directory):
            changes.append(action)
            os.remove(local_file.path)

    for local_file in list_documents_from_directory(directory):
        remote_file = remote_files_by_number.get(local_file.number)
        if remote_file:
            continue
        action = "ลบ %s-%s %s" % (local_file.number, local_file.revision, local_file.name)
        print(action, "...")
        changes.append(action)
        os.remove(local_file.path)

    return changes

def git_config(directory):
    subprocess.check_output(
        ["git", "config", "user.name", "DDC Bot"],
        cwd=directory,
    )
    subprocess.check_output(
        ["git", "config", "user.email", "ddc-bot@api.co.th"],
        cwd=directory,
    )
    subprocess.check_output(
        ["git", "config", "pull.rebase", "true"],
        cwd=directory,
    )


def git_pull(directory):
    try:
        subprocess.check_output(
            ["git", "pull", "origin", "main"],
            cwd=directory,
        )
    except:
        return

def git_commit(directory, message=None):
    subprocess.check_output(
        ["git", "add", "--all"],
        cwd=directory,
    )
    try:
        subprocess.check_output(
            ["git", "commit", "-a", "-m", '%s' % message],
            cwd=directory,
        )
    except:
        return

def git_push(directory):
    subprocess.check_output(
        ["git", "push", "origin", "main"],
        cwd=directory,
    )


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--directory', help='Git directory')

    args = parser.parse_args()
    directory = args.directory or config("GIT_DIRECTORY")

    git_pull(directory)
    changes = sync_directory(directory)
    if changes:
        git_commit(directory, message="\n".join(changes))
    git_push(directory)

if __name__ == '__main__':
    main()




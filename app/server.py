
import os
import datetime
from fastapi import FastAPI, BackgroundTasks
from fastapi.responses import RedirectResponse
from sync import sync_directory, git_pull, git_commit, git_config, git_push
from decouple import config


app = FastAPI(
    title='DDC Bot',
    description='Bot to sync between registered documents in bangbon.api.co.th and pdf files in github.com/api-th/documents',
)


GIT_DIRECTORY = config("GIT_DIRECTORY")

DDCBOT_LOCK_FILE = config("DDCBOT_LOCK_FILE", default=".ddcbot-sync.lock")
DDCBOT_LOCK_TIMEOUT_MINUTES = int(config("DDCBOT_LOCK_TIMEOUT_MINUTES", default=5))

def read_file(file_name):
    with open(file_name, "r") as f:
        content = ''.join(f.readlines())
    return content

def write_file(file_name, content=None):
    with open(file_name, "w") as f:
        f.write(content)

def acquire_lock(file_name, timeout_minutes=None):
	now = datetime.datetime.utcnow()
	if os.path.exists(file_name):
		content = read_file(file_name)
		expire_date = datetime.datetime.fromisoformat(content)
		if now <= expire_date:
			return False

	expire_date = now + datetime.timedelta(seconds=timeout_minutes * 60)
	write_file(file_name, content=expire_date.isoformat())
	return True

def release_lock(file_name):
	os.remove(file_name)

def lock(file_name, timeout_minutes=None):
	def decorator(function):
		def wrapper(*args, **kwargs):
			if not acquire_lock(file_name, timeout_minutes=timeout_minutes):
				return
			try:
				return function(*args, **kwargs)
			finally:
				release_lock(file_name)
		return wrapper
	return decorator

@lock(DDCBOT_LOCK_FILE, timeout_minutes=DDCBOT_LOCK_TIMEOUT_MINUTES)
def sync_repository():
	directory = GIT_DIRECTORY
	git_config(directory)
	git_pull(directory)
	all_changes = []
	while True:
		changes = sync_directory(directory)
		if not changes:
			break
		all_changes += changes
		git_commit(directory, message="\n".join(changes))
	git_push(directory)
	return all_changes

@app.get("/settings")
async def settings():
	return {
		"GIT_DIRECTORY": GIT_DIRECTORY,
		"DDCBOT_LOCK_FILE": DDCBOT_LOCK_FILE,
		"DDCBOT_LOCK_TIMEOUT_MINUTES": DDCBOT_LOCK_TIMEOUT_MINUTES,
	}

@app.post("/run")
async def run(background_tasks: BackgroundTasks):
    background_tasks.add_task(sync_repository)
    return {
		"result": "success",
    }

